#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#define TOUCH_FIFO  "touch_fifo"
#define FIFO_MODE   0644

#define PAGE_SIZE   512
#define NAME_SIZE   64

#define TIME_LIM    1000
#define TL_QUANT    100

char DIRECT_FIFO[NAME_SIZE] = {};

int follower();
int streamer( const char* src );

int main( int argc, const char* argv[] )
{
    int result = EXIT_SUCCESS;
    if( argc == 1 )
        result = follower();
    else if( argc == 2 )
        result = streamer( argv[1] );
    else
    {
        perror( "Too much arguments" );
        result = EXIT_FAILURE;
    }
    return result;
}

int open_touch_fifo( int flag )
{
    int touch_fd = -1;
    int mk_res = mkfifo( TOUCH_FIFO, FIFO_MODE );
    if( (mk_res != 0) && (errno != EEXIST) )
    {
        perror( "Can't create touch fifo" );
        return -1;
    }

    touch_fd = open( TOUCH_FIFO, flag );
    return touch_fd;
}

int write_pid()
{
    int touch_fd = -1;
    if( (touch_fd = open_touch_fifo( O_RDWR )) == -1 )
    {
        perror( "Can't open touch fifo" );
        return -1;
    }
    pid_t pid = getpid();
    int res = write( touch_fd, &pid, sizeof(pid_t) );

    return res;
}

int read_pid( pid_t* pid )
{
    int touch_fd = -1;
    if( (touch_fd = open_touch_fifo( O_RDWR )) == -1 )
    {
        perror( "Can't open touch fifo" );
        return -1;
    }

    int res = -1;
    res = read( touch_fd, pid, sizeof(pid_t));

    if( (res < sizeof(pid_t)) ||
        (*pid < 0) )
    { res = -1; }

    return res;
}

char* init_direct_fifo_name( pid_t pid )
{
    snprintf( DIRECT_FIFO, NAME_SIZE, "direct%d", (int)pid );
    return DIRECT_FIFO;
}

int follower()
{
    init_direct_fifo_name( getpid() );
    int mkf_res = mkfifo( DIRECT_FIFO, FIFO_MODE );
    if( (mkf_res != 0) && (errno != EEXIST) )
    {
        perror( "Can't create private fifo" );
        return EXIT_FAILURE;
    }

// start critical
    int direct_fd = open( DIRECT_FIFO, O_RDONLY | O_NONBLOCK );
    fcntl( direct_fd, F_SETFL, O_RDONLY );

    if( write_pid() != sizeof(pid_t) )
    {
        perror( "Can't send pid" );
        return EXIT_FAILURE;
    }

    if( ({  int nread = -1;
            for( int t = 0; t < TIME_LIM; t += TL_QUANT )
            {
                usleep( TL_QUANT );
                ioctl( direct_fd, FIONREAD, &nread );
                if( nread > 0 ) break;
            }
            nread; }) == 0 )
    {
        perror( "TL exceeded, assuming streamer is dead" );
        unlink( DIRECT_FIFO );
        return EXIT_FAILURE;
    }
// stop critical

    char buf[PAGE_SIZE] = {};
    int it_read = 0;
    while( (it_read = read( direct_fd, buf, PAGE_SIZE )) > 0 )
       write( STDOUT_FILENO, buf, it_read );

    unlink( DIRECT_FIFO );
    return EXIT_SUCCESS;
}

int streamer( const char* src )
{
    int src_fd = open( src, O_RDONLY );
    if( src_fd == -1 )
    {
        perror( "Can't open src file" );
        return EXIT_FAILURE;
    }

// start critical
    pid_t pid = -1;
    if( read_pid( &pid ) < 0 )
    {
        perror( "Invalid follower's pid" );
        return EXIT_FAILURE;
    }
    init_direct_fifo_name( pid );
    printf( "%s\n", DIRECT_FIFO );
    int direct_fd = open( DIRECT_FIFO, O_WRONLY | O_NONBLOCK );
    if( direct_fd == -1 )
    {
        perror( "Can't open private fifo" );
        return EXIT_FAILURE;
    }
    fcntl( direct_fd, F_SETFL, O_WRONLY );
// stop critical

    char buf[PAGE_SIZE] = {};
    int it_read = 0;
    while( (it_read = read( src_fd, buf, PAGE_SIZE )) > 0 )
        write( direct_fd, buf, it_read );

    close( src_fd );
    close( direct_fd );
    return EXIT_SUCCESS;
}

